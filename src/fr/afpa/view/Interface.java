package fr.afpa.view;
import java.text.ParseException;
import java.util.Scanner;

import fr.afpa.models.GestionBDD;
import fr.afpa.util.Impression;

public class Interface {
	
	
	/**
	 * Menu principal de l'interface client
	 * @throws ParseException : permet de relancer une éventuelle erreur à la couche supérieure (sert à relancer le menu dans le catch pour
	 * éviter un crash)
	 */
	public static void Menu () throws ParseException {
		Scanner in = new Scanner(System.in);
		String choix = "";
		do { 
			System.out.println("---------- [---SERVICE_DE_GESTION_CDA_BANQUE---] ---------- \n");
			System.out.println("Bonjour Monsieur Benjira, que souhaitez-vous faire ?\n");
			System.out.println
					(" 1 - Créer une agence\n"
					+ " 2 - Créer un client\n"
					+ " 3 - Créer un compte bancaire\n"
					+ " 4 - Rechercher un compte bancaire\n"
					+ " 5 - Rechercher un client\n"
					+ " 6 - Afficher la liste des comptes d'un client\n"
					+ " 7 - Imprimer la fiche d'informations d'un client\n"
					+ " 8 - Désactiver un client\n"
					+ " 9 - Supprimer un compte\n"
					+ "10 - Quitter le programme");
			
			choix = in.nextLine();
			
			switch (choix) {
			case "1" : GestionBDD.creerAgence(); break;
			case "2" : GestionBDD.creerClient(); break;
			case "3" : GestionBDD.creerCompteBancaire(); break;
			case "4" : GestionBDD.rechercherCompteBancaire(); break;
			case "5" : GestionBDD.rechercherClient(); break ;
			case "6" : GestionBDD.afficherComptesClient(); break;
			case "7" : Impression.genererFicheClient(); break;
			case "8" : GestionBDD.desactiverClient(); break;
			case "9" : GestionBDD.supprimerCompte(); break;
			case "10" :GestionBDD.quitterProgramme(); break;
			default : System.out.println("Commande inconnue, veuillez réessayer !"); break;
			}
			
		} while (choix != "10");
	}

}
