package fr.afpa.controls;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

public class Controle {
	
	
	/**
	 * Fonction permetatnt de vérifier la bonne syntaxe de l'identifiant client
	 * @param idClient = numéro d'identification du client
	 * @return La fonction retourne vrai si le format de l'identification client est correcte, sinon elle retourne un false.
	 */
	public static boolean checkIdClient (String idClient) {
		try {
			Pattern p = Pattern.compile("[A-Za-z]{2}[0-9]{6}");
			Matcher m = p.matcher(idClient);
			if (m.matches()) {
				return true;
			}
		} catch (PatternSyntaxException pse) {
			
		}
		return false;
	}
	
	
	public static boolean checkNumCompte (String numCompte) {
		try {
			Pattern p = Pattern.compile("[0-9]{11}");
			Matcher m = p.matcher(numCompte);
			if (m.matches()) {
				return true;
			}
		} catch (PatternSyntaxException pse) {
			
		}
		return false;
	}

	/**
	 * Fonction permettant de vérifier le nombre de comptes du client
	 * @param idClient = numéro d'identification du client
	 * @return La fonction retourne un true si le client possède déjà 3 comptes bancaires sinon elle renvoie un false.
	 */
	public static boolean checkNombreComptes (String idClient) {
			Connection conn = null;
			Statement stServices = null;
			ResultSet listeServices = null;
			int compteur = 0;
					
			try {
				String driverName = "org.postgresql.Driver";
				Class.forName(driverName);
				
				conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/employes","postgres","979wak1i");
				stServices = conn.createStatement();
				String strQuery = "SELECT count(c.nocompte) from compte c, client cl where cl.idclient = c.idclient and cl.idclient = '"+idClient+"'";
				compteur = stServices.executeUpdate(strQuery);
				
				
			} catch (ClassNotFoundException e) {
					e.printStackTrace();
				} 
				
				catch (SQLException e) {
					System.out.println("Le client a moins de 3 comptes bancaires enregistrés, vous pouvez continuer la procédure");
				}
				
				finally {
					
					if(listeServices != null) 
						try {
							listeServices.close();
						} catch (SQLException e) {
							e.printStackTrace();
						}
					
					if(stServices != null) 
						try {
							stServices.close();
						} catch (SQLException e) {
							e.printStackTrace();
						}
					
					if(conn != null) 
						try {
							conn.close();
						} catch (SQLException e) {
							e.printStackTrace();
						}
					
					
				}

			if (compteur == 3) {
				return true;
			}
			return false;
		
	}
}




