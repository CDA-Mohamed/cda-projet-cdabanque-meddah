package fr.afpa.util;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.util.Scanner;

import com.itextpdf.text.Document;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import fr.afpa.models.GestionBDD;
import fr.afpa.view.Interface;

public class Impression {
	
	
	
	public final static String DEST = "./Fiches_clients/fiche_client.pdf";
	private static Font catFont = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
	private static Font catFont2 = new Font(Font.FontFamily.TIMES_ROMAN, 14);	
	
	
	/**
	 * Fonction permettant de générer une fiche client au format PDF à partir d'un ResultSet
	 * @throws ParseException : renvoie une exception à la couche supérieur en cas d'erreur
	 */
	public static void genererFicheClient() throws ParseException {
		ResultSet listeServices = GestionBDD.infosFicheClient();
		
		
		try {		
			Document document = new Document();
			PdfWriter.getInstance(document, new FileOutputStream(DEST));
			document.open();
			FontFactory.getFont(FontFactory.TIMES_ROMAN, 18, Font.BOLD);
			
			Paragraph p1 = new Paragraph("Fiche Client", catFont);
			p1.setAlignment(p1.ALIGN_CENTER);
			p1.setFont(catFont);
			document.add(p1);
			addEmptyLine(p1,2);
			
			listeServices.next();
			Paragraph p2 = new Paragraph(" Numéro Client : "+listeServices.getString(1)+ "\n Nom : "+listeServices.getString(2)+ "\n Prénom : "+listeServices.getString(3)+
					" \n Date de naissance : " +listeServices.getDate(4), catFont2);		
			p2.setFont(catFont2);
			document.add(p2);	
			addEmptyLine(p2,2);
			
			Paragraph p3 = new Paragraph("__________________________________________________\nListe de compte\n__________________________________________________"
					+ "\nNuméro de compte               Solde\n__________________________________________________", catFont2);
			p3.setFont(catFont2);
			document.add(p3);
			
			listeServices.previous();
			while (listeServices.next()) {			
			String emote ="";
			
				if (listeServices.getFloat(6) < 0) {
					emote = ":-(";
				}
				else {
					emote = ":-)";
				}
				
				Paragraph p = new Paragraph(listeServices.getLong(5) + "                    "+listeServices.getFloat(6)+" euros                         "+emote);
				p.setFont(catFont2);
				document.add(p);
				
			}
			
			document.close();
			
		} 
		
		catch (Exception e) {
			e.printStackTrace();
	        //System.out.println("Un problème est survenue, vérifiez la destination du fichier et réessayer.");
	    }
			
	}

	private static void addEmptyLine(Paragraph p2, int i) {
		// TODO Auto-generated method stub
		
	}

}
