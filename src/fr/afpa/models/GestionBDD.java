package fr.afpa.models;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Scanner;

import fr.afpa.controls.Controle;
import fr.afpa.util.Impression;
import fr.afpa.view.Interface;

public class GestionBDD {
	
static Scanner in = new Scanner(System.in);

/**
 * Fonction permettant de créer une agence à l'aide d'une connexion via la base de données
 * @throws ParseException
 */

public static void creerAgence() throws ParseException {
	
	Connection conn = null;
	Statement stServices = null;
	String nomAgence = "";
	String adresseAgence = "";
	
	System.out.println("Quel est le nom de l'agence ?");
	nomAgence = in.nextLine();
	
	System.out.println("Quelle est l'adresse de l'agence ?");
	adresseAgence = in.nextLine();
			
	
	try {
		String driverName = "org.postgresql.Driver";
		Class.forName(driverName);
		
		conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/CDABANQUE","postgres","979wak1i");
		stServices = conn.createStatement();
		String strQuery = "INSERT INTO agence VALUES(nextval('codeagence'),'"+nomAgence+"','"+adresseAgence+"')";
		stServices.executeUpdate(strQuery);
		
	} catch (ClassNotFoundException e) {
			e.printStackTrace();
			//System.out.println("La commande a rencontré un problème. Veuillez vérifier la base de données et réessayer.");
			//Interface.Menu();
		} 
		
		catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			
			if(stServices != null) 
				try {
					stServices.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			
			if(conn != null) 
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}
	
	in.nextLine();
		
	}

/**
 * Fonction permettant de créer un client à l'aide d'une connexion via la base de données
 * @throws ParseException
 */

public static void creerClient() throws ParseException {
	Connection conn = null;
	Statement stServices = null;
	String idClient = "";
	String nomClient = "";
	String prenomClient = "";
	String adresseClient = "";
	Date naissanceClient;
	String emailClient = "";
	boolean actif = false;
	String reponse = "";
	int codeAgence;
	
	do {System.out.println("Donnez un identifiant à votre client");
		idClient = in.nextLine();
		if (Controle.checkIdClient(idClient) == false) {
			System.out.println("Vérifiez la syntaxte. L'identifiant doit comporter 2 lettres et 6 chiffres");
		}
	} while (Controle.checkIdClient(idClient) == false);
	
	System.out.println("Quelle est le nom de votre client ?");
	nomClient = in.nextLine();
	
	System.out.println("Quelle est le prénom de votre client ?");
	prenomClient = in.nextLine();
	

	System.out.println("Quelle est la date de naissance de votre client ?(veuillez respecter le format jj-mm-aaaa)");
	SimpleDateFormat formatter = new SimpleDateFormat("dd-M-yyyy", Locale.FRENCH);
	String date = in.nextLine();
	naissanceClient = formatter.parse(date);
	
	System.out.println("Quelle est l'adresse e-mail de votre client ?");
	emailClient = in.nextLine();
	
	System.out.println("Quelle est l'adresse de votre client ?");
	adresseClient = in.nextLine();
	
	do {
	System.out.println("Votre client sera t-il actif ? (Oui/Non)");
	reponse = in.nextLine();
	if (reponse.equalsIgnoreCase("oui")) {
		actif = true;
	}
	else if (reponse.equalsIgnoreCase("non")) {
		actif = false;
	}
	
	else {
		System.out.println("Réponse non valide. Veuillez répondre par Oui ou Non.");
	}
	} while (!reponse.equalsIgnoreCase("oui") && !reponse.equalsIgnoreCase("non"));
		
	System.out.println("A quel numéro d'agence voulez-vous rattacher votre client ?");
	codeAgence = in.nextInt();
	
	try {
		String driverName = "org.postgresql.Driver";
		Class.forName(driverName);
		
		conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/CDABANQUE","postgres","979wak1i");
		stServices = conn.createStatement();
		String strQuery = "INSERT INTO client VALUES('"+idClient+"','"+nomClient+"','"+prenomClient+"','"+naissanceClient+"','"+emailClient+"',"
				+ "'"+adresseClient+"', '"+actif+"','"+codeAgence+"')";
		stServices.executeUpdate(strQuery);
		
	} catch (ClassNotFoundException e) {
		System.out.println("La commande a rencontré un problème. Veuillez vérifier la base de données et réessayer.");
		Interface.Menu();
		} 
		
		catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			
			if(stServices != null) 
				try {
					stServices.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			
			if(conn != null) 
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}
	
	in.nextLine();
			
	
	}

/**
 * Fonction permettant de créer un compte bancaire à l'aide d'une connexion via la base de données
 * @throws ParseException
 */

public static void creerCompteBancaire() throws ParseException {
	
	Connection conn = null;
	Statement stServices = null;
	int solde;
	boolean decouvert = false;
	int codeAgence;
	String idClient = "";
	String reponse = "";
	
	do {System.out.println("A quel identifiant client sera rattaché ce compte ?");
	idClient = in.nextLine();
	if (Controle.checkIdClient(idClient) == false) {
		System.out.println("Vérifiez la syntaxte. L'identifiant doit comporter 2 lettres et 6 chiffres");
	}
} while (Controle.checkIdClient(idClient) == false);
	
	if (Controle.checkNombreComptes(idClient))  {
		System.out.println("Désolé, le client possède déjà la limite de comptes autorisés.");
	}
	
	else {
	
	System.out.println("Quel est le code agence de l'agence où se trouvera le compte ?");
	codeAgence = in.nextInt();
	
	System.out.println("Quel est le solde de départ du compte ?");
	solde = in.nextInt();
	
	do {
		System.out.println(" Ce compte aura t-il un découvert autorisé ? (Oui/Non)");
		reponse = in.next();
		if (reponse.equalsIgnoreCase("oui")) {
			decouvert = true;
		}
		else if (reponse.equalsIgnoreCase("non")) {
			decouvert = false;
		}
		
		else {
			System.out.println("Réponse non valide. Veuillez répondre par Oui ou Non.");
		}
		
		} while (!reponse.equalsIgnoreCase("oui") && !reponse.equalsIgnoreCase("non"));
	
	
	try {
		String driverName = "org.postgresql.Driver";
		Class.forName(driverName);
		
		conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/CDABANQUE","postgres","979wak1i");
		stServices = conn.createStatement();
		String strQuery = "INSERT INTO compte VALUES(nextval('nocompte'),"+solde+","+decouvert+",'"+idClient+"',"+codeAgence+")";
		stServices.executeUpdate(strQuery);
		
	} catch (ClassNotFoundException e) {
		System.out.println("La commande a rencontré un problème. Veuillez vérifier la base de données et réessayer.");
		Interface.Menu();
			
		} 
		
		catch (SQLException e) {
			System.out.println("Le compte n'a pas pu être crée. Vérifiez que le code agence et/ou l'identifiant client existe");
			Interface.Menu();
		}
		
		finally {
			
			if(stServices != null) 
				try {
					stServices.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			
			if(conn != null) 
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}
	}
	in.nextLine();
		
	
}

/**
 * Fonction permettant de rechercher un compte bancaire à l'aide d'une connexion via la base de données en entrant le numéro de compte
 * @throws ParseException
 */

public static void rechercherCompteBancaire() throws ParseException {
	
	Connection conn = null;
	Statement stServices = null;
	ResultSet listeServices = null;
	String numCompte = "";
	
	do {System.out.println("Entrer le numéro de compte a retrouver");
	numCompte = in.nextLine();
	if (Controle.checkNumCompte(numCompte) == false) {
		System.out.println("Vérifiez la syntaxte. Le numéro de comptee doit comporter 11 chiffres");
	}
} while (Controle.checkNumCompte(numCompte) == false);
	
	try {
		String driverName = "org.postgresql.Driver";
		Class.forName(driverName);
		
		conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/CDABANQUE","postgres","979wak1i");
		stServices = conn.createStatement();
		String strQuery = "SELECT * FROM compte where nocompte ="+numCompte+"";
		listeServices = stServices.executeQuery(strQuery);
		
		while (listeServices.next()) {
			System.out.println("----------------------------------------------");
			System.out.println("Numero de compte : " + listeServices.getLong(1));
			System.out.println("Solde du compte : " + listeServices.getInt(2));
			if (listeServices.getBoolean(3) == true) {
			System.out.println("Découvert autorisé : Oui");
			}
			else {
				System.out.println("Découvert autorisé : Non");	
			}
			System.out.println("Identifiant client : " + listeServices.getString(4));
			System.out.println("Code Agence : " + listeServices.getInt(5));
			System.out.println("----------------------------------------------");
			
		}
		
	} catch (ClassNotFoundException e) {
		System.out.println("La commande a rencontré un problème. Veuillez vérifier la base de données et réessayer.");
		Interface.Menu();
		} 
		
		catch (SQLException e) {
			System.out.println("Désolé, le compte n'a pas été retrouvé, vérifier le numéro de compte.");
			Interface.Menu();
		}
		
		finally {
			
			if(listeServices != null) 
				try {
					listeServices.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			
			if(stServices != null) 
				try {
					stServices.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			
			if(conn != null) 
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
	
		}
	
}

/**
 * Fonction permettant de rechercher un client à l'aide d'une connexion via la base de données de son nom, de son compte ou de son identifiant
 * @throws ParseException
 */

public static void rechercherClient() throws ParseException {
	
	Connection conn = null;
	Statement stServices = null;
	ResultSet listeServices = null;
	String reponse = "";
	String nomClient = "";
	String numCompte = "";
	String idClient = "";
	
	
	System.out.println("Voulez-vous recherchez votre client :\n"
			+ "1 - Par son nom.\n"
			+ "2 - Par son numéro de compte.\n"
			+ "3 - Par son identifiant.");
	
	reponse = in.nextLine();
	
	switch(reponse) {
	case "1" : 

	System.out.println("Entrez le nom de votre client");
	nomClient = in.nextLine();
	
	try {
		String driverName = "org.postgresql.Driver";
		Class.forName(driverName);
		
		conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/CDABANQUE","postgres","979wak1i");
		stServices = conn.createStatement();
		String strQuery = "SELECT * FROM client where nom = '"+nomClient+"' and actif = 'true'";
		listeServices = stServices.executeQuery(strQuery);
		
		while (listeServices.next()) {
			System.out.println("----------------------------------------------");
			System.out.println("Identifiant : " + listeServices.getString(1));
			System.out.println("Nom : " + listeServices.getString(2));
			System.out.println("Prenom : " + listeServices.getString(3));
			System.out.println("Date de Naissance : " + listeServices.getDate(4));
			System.out.println("Email : " + listeServices.getString(5));
			System.out.println("Adresse : " + listeServices.getString(6));
			System.out.println("Code Agence : " + listeServices.getInt(8));
			System.out.println("----------------------------------------------");
			
		}
		
	} catch (ClassNotFoundException e) {
		System.out.println("Désolé, une erreur s'est produite, veuillez vérifier la base de données et réessayer.");
		Interface.Menu();
		} 
		
		catch (SQLException e) {
			System.out.println("Désolé, le client n'a pas été retrouvé, vérifier que le nom soit correct.");
			Interface.Menu();
		}
		
		finally {
			
			if(listeServices != null) 
				try {
					listeServices.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			
			if(stServices != null) 
				try {
					stServices.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			
			if(conn != null) 
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
	
		}
	break;
	
	case "2" :  System.out.println("Entrez le numero de compte de votre client");
	numCompte = in.nextLine();
	
	try {
		String driverName = "org.postgresql.Driver";
		Class.forName(driverName);
		
		conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/CDABANQUE","postgres","979wak1i");
		stServices = conn.createStatement();
		String strQuery = "SELECT cl.* FROM client cl, compte c where c.nocompte = '"+numCompte+"' and actif = 'true' and cl.idclient = c.idclient";
		listeServices = stServices.executeQuery(strQuery);
		
		while (listeServices.next()) {
			System.out.println("----------------------------------------------");
			System.out.println("Identifiant : " + listeServices.getString(1));
			System.out.println("Nom : " + listeServices.getString(2));
			System.out.println("Prenom : " + listeServices.getString(3));
			System.out.println("Date de Naissance : " + listeServices.getDate(4));
			System.out.println("Email : " + listeServices.getString(5));
			System.out.println("Adresse : " + listeServices.getString(6));
			System.out.println("Code Agence : " + listeServices.getInt(8));
			System.out.println("----------------------------------------------");
			
		}
		
	} catch (ClassNotFoundException e) {
		System.out.println("Désolé, une erreur s'est produite, veuillez vérifier la base de données et réessayer.");
		Interface.Menu();
		} 
		
		catch (SQLException e) {
			System.out.println("Désolé, le client n'a pas été retrouvé, vérifier que le numéro de compte soit correct.");
			Interface.Menu();
		}
		
		finally {
			
			if(listeServices != null) 
				try {
					listeServices.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			
			if(stServices != null) 
				try {
					stServices.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			
			if(conn != null) 
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
	
		}
	break;
	
	case "3" :
		do {
			System.out.println("Entrez l'identifiant de votre client");
			idClient = in.nextLine();
			if (!Controle.checkIdClient(idClient)) {
				System.out.println("L'identifiant client doit comporter 2 lettres et 6 chiffres, veuillez réessayer");
			}
			
			else {
	    
	
	try {
		String driverName = "org.postgresql.Driver";
		Class.forName(driverName);
		
		conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/CDABANQUE","postgres","979wak1i");
		stServices = conn.createStatement();
		String strQuery = "SELECT * FROM client where idclient = '"+idClient+"' and actif = 'true'";
		listeServices = stServices.executeQuery(strQuery);
		
		while (listeServices.next()) {
			System.out.println("----------------------------------------------");
			System.out.println("Identifiant : " + listeServices.getString(1));
			System.out.println("Nom : " + listeServices.getString(2));
			System.out.println("Prenom : " + listeServices.getString(3));
			System.out.println("Date de Naissance : " + listeServices.getDate(4));
			System.out.println("Email : " + listeServices.getString(5));
			System.out.println("Adresse : " + listeServices.getString(6));
			System.out.println("Code Agence : " + listeServices.getInt(8));
			System.out.println("----------------------------------------------");
			
		}
		
	} catch (ClassNotFoundException e) {
			System.out.println("Désolé, une erreur s'est produite, veuillez vérifier la base de données et réessayer.");
			Interface.Menu();
		} 
		
		catch (SQLException e) {
			System.out.println("Désolé, le client n'a pas été retrouvé, vérifier que l'identifiant soit correct.");
			Interface.Menu();
		}
		
		finally {
			
			if(listeServices != null) 
				try {
					listeServices.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			
			if(stServices != null) 
				try {
					stServices.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			
			if(conn != null) 
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
	
		}
			}
		}while(Controle.checkIdClient(idClient));
	break;
	
	
	default : System.out.println("Commande inconnue, veuillez réessayer."); break;
	
	}
}

/**
 * Fonction permettant d'afficher les comptes d'un client à l'aide d'une connexion via la base de données par son identifiant
 * @throws ParseException
 */

public static void afficherComptesClient() throws ParseException {
	Connection conn = null;
	Statement stServices = null;
	ResultSet listeServices = null;
	String idClient = "";
	
	System.out.println("Entrez l'identifiant de votre client");
    idClient = in.nextLine();

try {
	String driverName = "org.postgresql.Driver";
	Class.forName(driverName);
	
	conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/CDABANQUE","postgres","979wak1i");
	stServices = conn.createStatement();
	String strQuery = "SELECT c.* FROM compte c, client cl where c.idclient = '"+idClient+"' and cl.actif = 'true' and cl.idclient = c.idclient";
	listeServices = stServices.executeQuery(strQuery);
	
	while (listeServices.next()) {
		System.out.println("----------------------------------------------");
		System.out.println("Numéro de compte : " + listeServices.getLong(1));
		System.out.println("Solde : " + listeServices.getFloat(2));
		if (listeServices.getBoolean(3) == true) {
			System.out.println("Découvert autorisé : Oui");
			}
			else {
				System.out.println("Découvert autorisé : Non");	
			}
		System.out.println("Identifiant client : " + listeServices.getString(4));
		System.out.println("Code Agence : " + listeServices.getInt(5));
		System.out.println("----------------------------------------------");
		
	}
	
} catch (ClassNotFoundException e) {
	System.out.println("Désolé, une erreur s'est produite, veuillez vérifier la base de données et réessayer.");
	Interface.Menu();
	} 
	
	catch (SQLException e) {
		System.out.println("Désolé, le client n'a pas été retrouvé, veuillez vérifier l'identifiant et réessayer.");
		Interface.Menu();
	}
	
	finally {
		
		if(listeServices != null) 
			try {
				listeServices.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		
		if(stServices != null) 
			try {
				stServices.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		
		if(conn != null) 
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}

	}
	
}

/**
 * Fonction permettant de génerer les infos d'un client à l'aide d'une connexion via la base de données pour l'envoyer au service de génération de PDF
 * @return : la fonction retourne un ResultSet contenant les informations du client contenues dans la BDD
 * @throws ParseException
 */

public static ResultSet infosFicheClient() throws ParseException {
	Connection conn = null;
	Statement stServices = null;
	ResultSet listeServices = null;
	String idClient = "";
	

	System.out.println("Entrez l'identifiant de votre client");
	idClient = in.nextLine();
	
	try {
		String driverName = "org.postgresql.Driver";
		Class.forName(driverName);
		
		conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/CDABANQUE","postgres","979wak1i");
		stServices = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
		String strQuery = "SELECT cl.idclient,cl.nom,cl.prenom,cl.naissance, c.nocompte, c.solde FROM client cl, compte c where c.idclient = cl.idclient and cl.idclient = '"+idClient+"'";
		listeServices = stServices.executeQuery(strQuery);
	}
		catch (ClassNotFoundException e) {
	System.out.println("Désolé, une erreur s'est produite, veuillez vérifier la base de données et réessayer.");
	Interface.Menu();
		} 
	
		catch (SQLException e) {
		System.out.println("Désolé, le client n'a pas été retrouvé, vérifier que le nom soit correct.");
		Interface.Menu();
	
		}
	finally {
		
		if(stServices != null) 
			try {
				stServices.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		
		if(conn != null) 
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}

	}
	return listeServices;
	
}

/**
 * Fonction qui permet de desactiver un client afin de ne plus rendre visible ses informations ni ses comptes.
 * @throws ParseException
 */

public static void desactiverClient () throws ParseException {
	
	Connection conn = null;
	Statement stServices = null;
	String idClient = "";
	
	System.out.println("Entrez l'identifiant client du client que vous voulez desactiver");
	idClient = in.nextLine();
	
	try {
		String driverName = "org.postgresql.Driver";
		Class.forName(driverName);
		
		conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/CDABANQUE","postgres","979wak1i");
		stServices = conn.createStatement();
		String strQuery = "UPDATE client set actif = 'false' where idclient = '"+idClient+"'";
		stServices.executeUpdate(strQuery);
		
		
	} catch (ClassNotFoundException e) {
		System.out.println("Désolé, le client n'a pas été retrouvé, veuillez vérifier l'identifiant et réessayer.");
		Interface.Menu();
		} 
		
		catch (SQLException e) {
			System.out.println("Désolé, une erreur s'est produite, veuillez vérifier la base de données et réessayer.");
			Interface.Menu();
		}
		
		finally {
			
			if(stServices != null) 
				try {
					stServices.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			
			if(conn != null) 
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}
	
		
	}
	
/**
 * Fonction permettant de supprimer un compte bancaire à l'aide du numéro de compte
 * @throws ParseException
 */

public static void supprimerCompte() throws ParseException {
	Connection conn = null;
	Statement stServices = null;
	String numCompte = "";
	
	System.out.println("Entrez le numero de compte que vous voulez supprimer");
	numCompte = in.nextLine();
	
	try {
		String driverName = "org.postgresql.Driver";
		Class.forName(driverName);
		
		conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/CDABANQUE","postgres","979wak1i");
		stServices = conn.createStatement();
		String strQuery = "DELETE FROM compte where nocompte = '"+numCompte+"'";
		stServices.executeUpdate(strQuery);
		
	} catch (ClassNotFoundException e) {
		System.out.println("Désolé, une erreur s'est produite, veuillez vérifier la base de données et réessayer.");
		Interface.Menu();
		} 
		
		catch (SQLException e) {
			System.out.println("Désolé, le compte n'a pas été retrouvé, veuillez vérifier le numéro de compte et réessayer.");
			Interface.Menu();
		}
		
		finally {
			
			if(stServices != null) 
				try {
					stServices.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			
			if(conn != null) 
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}
	
	
	
}

public static void quitterProgramme() {
	System.out.println("Vous avez quitté l'application. A bientôt !");
	System.exit(-1);
}

}
