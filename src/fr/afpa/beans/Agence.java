package fr.afpa.beans;

public class Agence {
	
	private int codeAgence;
	private String nom;
	private String adresse;
	
	public Agence (int codeAgence, String nom, String adresse) {
		codeAgence = this.codeAgence;
		nom = this.nom;
		adresse = this.adresse;
	}

	public int getCodeAgence() {
		return codeAgence;
	}

	public void setCodeAgence(int codeAgence) {
		this.codeAgence = codeAgence;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}
	
	

}
