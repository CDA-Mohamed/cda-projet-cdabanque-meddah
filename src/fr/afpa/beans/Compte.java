package fr.afpa.beans;

public class Compte {
	private long numCompte;
	private float solde;
	private boolean decouvert;
	
	public Compte (long numCompte, float solde, boolean decouvert) {
		numCompte = this.numCompte;
		solde = this.solde;
		decouvert = this.decouvert;
	}

	public long getNumCompte() {
		return numCompte;
	}

	public void setNumCompte(long numCompte) {
		this.numCompte = numCompte;
	}

	public float getSolde() {
		return solde;
	}

	public void setSolde(float solde) {
		this.solde = solde;
	}

	public boolean isDecouvert() {
		return decouvert;
	}

	public void setDecouvert(boolean decouvert) {
		this.decouvert = decouvert;
	}
	
	

}
