package fr.afpa.beans;

import java.time.LocalDate;

public class Client {
	private String idClient;
	private String nom;
	private String prenom;
	private LocalDate dateNaissance;
	private String email;
	private boolean actif;
	private Compte [] comptes;
	
	public Client(String idClient, String nom, String prenom, LocalDate dateNaissance, String email, boolean actif, Compte [] comptes) {
		idClient = this.idClient;
		nom = this.nom;
		prenom = this.prenom;
		dateNaissance = this.dateNaissance;
		email = this.email;
		actif = true;
		comptes = new Compte[3];
		
	}

	public Client() {
		// TODO Auto-generated constructor stub
	}

	public String getIdClient() {
		return idClient;
	}

	public void setIdClient(String idClient) {
		this.idClient = idClient;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public LocalDate getDateNaissance() {
		return dateNaissance;
	}

	public void setDateNaissance(LocalDate dateNaissance) {
		this.dateNaissance = dateNaissance;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isActif() {
		return actif;
	}

	public void setActif(boolean actif) {
		this.actif = actif;
	}

	public Compte [] getComptes() {
		return comptes;
	}

	public void setComptes(Compte [] comptes) {
		this.comptes = comptes;
	}
	
	

}
